#pragma once

#include <iostream>
#include <array>
#include "Timer.h"

template<class P>
struct FilterObject {
  P Xprev [5];
  P Cprev [15];
  P X [5];
  P C [15];
  P Xref [5];
  P H [5];
  P refResidual;
  P errorMeas2;
  P chi2;
};

template<>
struct FilterObject<float> {
  FilterObject() = default;
  FilterObject(const FilterObject<float>& f) = default;
  FilterObject(const FilterObject<double>& fo) {
    auto assign5 = [] (float to [5], const double from [5]) { for(int i=0; i<5; ++i) to[i] = from[i]; };
    auto assign15 = [] (float to [15], const double from [15]) { for(int i=0; i<15; ++i) to[i] = from[i]; };
    
    assign5(Xprev, fo.Xprev);
    assign15(Cprev, fo.Cprev);
    assign5(X, fo.X);
    assign15(C, fo.C);
    assign5(Xref, fo.Xref);
    assign5(H, fo.H);
    refResidual = fo.refResidual;
    errorMeas2 = fo.errorMeas2;
    chi2 = fo.chi2;
  }

  float Xprev [5];
  float Cprev [15];
  float X [5];
  float C [15];
  float Xref [5];
  float H [5];
  float refResidual;
  float errorMeas2;
  float chi2;
};

typedef FilterObject<double> Filterd;
typedef FilterObject<float>  Filterf;

// Templated filter
template<class T, class T5, class T15>
__attribute__ ((noinline)) T templatedFilter(
  T5& X,
  T15& C,
  const T5& Xref,
  const T5& H,
  const T& refResidual,
  const T& errorMeas2
) {
  const auto res = refResidual +  H[0] * (Xref[0] - X[0]) 
                         +  H[1] * (Xref[1] - X[1])
                         +  H[2] * (Xref[2] - X[2])
                         +  H[3] * (Xref[3] - X[3]) 
                         +  H[4] * (Xref[4] - X[4]); // -8.0083058873387498e-07
  
  T5 CHT = {
   C[ 0]*H[0] + C[ 1]*H[1] + C[ 3]*H[2] + C[ 6]*H[3] + C[10]*H[4] ,
   C[ 1]*H[0] + C[ 2]*H[1] + C[ 4]*H[2] + C[ 7]*H[3] + C[11]*H[4] ,
   C[ 3]*H[0] + C[ 4]*H[1] + C[ 5]*H[2] + C[ 8]*H[3] + C[12]*H[4] ,
   C[ 6]*H[0] + C[ 7]*H[1] + C[ 8]*H[2] + C[ 9]*H[3] + C[13]*H[4] ,
   C[10]*H[0] + C[11]*H[1] + C[12]*H[2] + C[13]*H[3] + C[14]*H[4] 
  }; // {0, -399.94664979632779, 0, 0, 0}

  const auto errorRes2  = errorMeas2 + H[0]*CHT[0] 
                                + H[1]*CHT[1] 
                                + H[2]*CHT[2]
                                + H[3]*CHT[3] 
                                + H[4]*CHT[4] ;

  const auto w0 = res/errorRes2; // -2.0026098521581774e-09
  X[0] += CHT[0] * w0 ;
  X[1] += CHT[1] * w0 ;
  X[2] += CHT[2] * w0 ;
  X[3] += CHT[3] * w0 ;
  X[4] += CHT[4] * w0 ;

  const auto w1 = 1./errorRes2;
  C[ 0] -= w1 * CHT[0] * CHT[0] ;
  C[ 1] -= w1 * CHT[1] * CHT[0] ;
  C[ 3] -= w1 * CHT[2] * CHT[0] ;
  C[ 6] -= w1 * CHT[3] * CHT[0] ;
  C[10] -= w1 * CHT[4] * CHT[0] ;

  C[ 2] -= w1 * CHT[1] * CHT[1] ;
  C[ 4] -= w1 * CHT[2] * CHT[1] ;
  C[ 7] -= w1 * CHT[3] * CHT[1] ;
  C[11] -= w1 * CHT[4] * CHT[1] ;

  C[ 5] -= w1 * CHT[2] * CHT[2] ;
  C[ 8] -= w1 * CHT[3] * CHT[2] ;
  C[12] -= w1 * CHT[4] * CHT[2] ;

  C[ 9] -= w1 * CHT[3] * CHT[3] ;
  C[13] -= w1 * CHT[4] * CHT[3] ;

  C[14] -= w1 * CHT[4] * CHT[4] ;

  return res*res / errorRes2;
}
