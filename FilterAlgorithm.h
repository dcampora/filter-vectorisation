#pragma once

#include "vectorclass/vectorclass.h"
#include "Timer.h"
#include <malloc.h>

#define FILTER(_fo) templatedFilter(_fo.Xprev, _fo.Cprev, _fo.Xref, _fo.H, _fo.refResidual, _fo.errorMeas2)

template<class P>
class FilterAlgorithm {
protected:
  std::vector<FilterObject<P>> original;
  std::vector<FilterObject<P>> instance;

public:
  P* chi2;
  std::vector<Timer> timers;

  FilterAlgorithm(){}
  FilterAlgorithm(const std::vector<FilterObject<P>>& filterobjects) { setInstance(filterobjects); }
  ~FilterAlgorithm(){
    if (instance.size() > 1) free(chi2);
  }

  void setInstance(const std::vector<FilterObject<P>>& filterobjects) {
    if (instance.size() > 1) free(chi2);
    original = filterobjects;
    instance = original;
    chi2 = (P*) memalign(64, instance.size() * sizeof(P));
  }

  void reset() {
    if (instance.size() > 1) free(chi2);
    instance = original;
    chi2 = (P*) memalign(64, instance.size() * sizeof(P));
  }

  void resetTimers() {
    timers.clear();
  }
  
  virtual void operator()() = 0;
};

template<class P>
class FilterSequential : public FilterAlgorithm<P> {
public:
  FilterSequential(){}
  FilterSequential(const std::vector<FilterObject<P>>& filterobjects) 
    : FilterAlgorithm<P>(filterobjects) {}

  void operator()();
};

template<>
void FilterSequential<double>::operator()() {
  reset();
  timers.push_back(Timer());
  timers.back().start();
  for (int i=0; i<instance.size(); ++i) {
    chi2[i] = FILTER(instance[i]);
  }
  timers.back().stop();
};
template<>
void FilterSequential<float>::operator()() {
  reset();
  timers.push_back(Timer());
  timers.back().start();
  for (int i=0; i<instance.size(); ++i) {
    chi2[i] = FILTER(instance[i]);
  }
  timers.back().stop();
};


class FilterOriginalIntrinsics : public FilterAlgorithm<double> {
public:
  std::vector<double> cs;
  std::vector<double> xs;

  FilterOriginalIntrinsics(){}
  FilterOriginalIntrinsics(const std::vector<Filterd>& filterobjects) 
    : FilterAlgorithm<double>(filterobjects) {
    cs = std::vector<double>(15 * instance.size());
    xs = std::vector<double>(5 * instance.size());
  }

  void operator()() {
    reset();
    timers.push_back(Timer());
    timers.back().start();
    for (int i=0; i<instance.size()/2; ++i) {
      originalIntrinsicsFilter(chi2 + i*2, cs.data() + i*2*15, xs.data() + i*2*5, instance[i*2], instance[i*2 + 1]);
    }
    timers.back().stop();
  }
};

template<class P, class ... F>
class FilterVectorised : public FilterAlgorithm<P> {
private:
  int vectorWidth;
  void (*f) (P *, F&...);

public:
  FilterVectorised(){}
  FilterVectorised(const std::vector<FilterObject<P>>& filterobjects) 
    : FilterAlgorithm<P>(filterobjects) {}
  FilterVectorised(const std::vector<FilterObject<P>>& filterobjects, void (*function) (P *, F&...)) 
    : FilterAlgorithm<P>(filterobjects) {
      setupVectorised(function);
    }

  void setupVectorised(void (*function) (P *, F&...)) {
    vectorWidth = sizeof...(F);
    f = function;
  }

  void operator()(){}
};

template<>
void FilterVectorised<double, Filterd, Filterd>::operator()() {
  reset();
  timers.push_back(Timer());
  timers.back().start();
  for (int i=0; i<instance.size()/vectorWidth; ++i) {
    f(chi2 + i*vectorWidth, instance[i*vectorWidth], instance[i*vectorWidth + 1]);
  }
  timers.back().stop();
}

template<>
void FilterVectorised<double, Filterd, Filterd, Filterd, Filterd>::operator()() {
  reset();
  timers.push_back(Timer());
  timers.back().start();
  for (int i=0; i<instance.size()/vectorWidth; ++i) {
    f(chi2 + i*vectorWidth, instance[i*vectorWidth], instance[i*vectorWidth + 1], instance[i*vectorWidth + 2], instance[i*vectorWidth + 3]);
  }
  timers.back().stop();
}
template<>
void FilterVectorised<float, Filterf, Filterf, Filterf, Filterf>::operator()() {
  reset();
  timers.push_back(Timer());
  timers.back().start();
  for (int i=0; i<instance.size()/vectorWidth; ++i) {
    f(chi2 + i*vectorWidth, instance[i*vectorWidth], instance[i*vectorWidth + 1], instance[i*vectorWidth + 2], instance[i*vectorWidth + 3]);
  }
  timers.back().stop();
}

template<>
void FilterVectorised<double, Filterd, Filterd, Filterd, Filterd, Filterd, Filterd, Filterd, Filterd>::operator()() {
  reset();
  timers.push_back(Timer());
  timers.back().start();
  for (int i=0; i<instance.size()/vectorWidth; ++i) {
    f(chi2 + i*vectorWidth, instance[i*vectorWidth], instance[i*vectorWidth + 1], instance[i*vectorWidth + 2], instance[i*vectorWidth + 3],
      instance[i*vectorWidth + 4], instance[i*vectorWidth + 5], instance[i*vectorWidth + 6], instance[i*vectorWidth + 7]);
  }
  timers.back().stop();
}
template<>
void FilterVectorised<float, Filterf, Filterf, Filterf, Filterf, Filterf, Filterf, Filterf, Filterf>::operator()() {
  reset();
  timers.push_back(Timer());
  timers.back().start();
  for (int i=0; i<instance.size()/vectorWidth; ++i) {
    f(chi2 + i*vectorWidth, instance[i*vectorWidth], instance[i*vectorWidth + 1], instance[i*vectorWidth + 2], instance[i*vectorWidth + 3],
      instance[i*vectorWidth + 4], instance[i*vectorWidth + 5], instance[i*vectorWidth + 6], instance[i*vectorWidth + 7]);
  }
  timers.back().stop();
}

template<>
void FilterVectorised<float, Filterf, Filterf, Filterf, Filterf, Filterf, Filterf, Filterf, Filterf, 
                             Filterf, Filterf, Filterf, Filterf, Filterf, Filterf, Filterf, Filterf>::operator()() {
  reset();
  timers.push_back(Timer());
  timers.back().start();
  for (int i=0; i<instance.size()/vectorWidth; ++i) {
    f(chi2 + i*vectorWidth, instance[i*vectorWidth], instance[i*vectorWidth + 1], instance[i*vectorWidth + 2], instance[i*vectorWidth + 3],
      instance[i*vectorWidth + 4], instance[i*vectorWidth + 5], instance[i*vectorWidth + 6], instance[i*vectorWidth + 7],
      instance[i*vectorWidth + 8], instance[i*vectorWidth + 9], instance[i*vectorWidth + 10], instance[i*vectorWidth + 11],
      instance[i*vectorWidth + 12], instance[i*vectorWidth + 13], instance[i*vectorWidth + 14], instance[i*vectorWidth + 15]);
  }
  timers.back().stop();
}

#if INSTRSET >= 7

#include "Vertical.h"
#define FILTER_VERTICALLYVECTORISED(_fo) filterVerticallyVectorised(_fo.Xprev, _fo.Cprev, _fo.Xref, _fo.H, _fo.refResidual, _fo.errorMeas2)
class FilterVerticallyVectorised : public FilterAlgorithm<double> {
public:
  FilterVerticallyVectorised(){}
  FilterVerticallyVectorised(const std::vector<Filterd>& filterobjects) 
    : FilterAlgorithm<double>(filterobjects) {}

  void operator()() {
    reset();
    timers.push_back(Timer());
    timers.back().start();
    for (int i=0; i<instance.size(); ++i) {
      chi2[i] = FILTER_VERTICALLYVECTORISED(instance[i]);
    }
    timers.back().stop();
  }
};

class FilterIntrinsics : public FilterAlgorithm<double> {
public:
  std::vector<double> cs;
  std::vector<double> xs;

  FilterIntrinsics(){}
  FilterIntrinsics(const std::vector<Filterd>& filterobjects) 
    : FilterAlgorithm<double>(filterobjects) {
    cs = std::vector<double>(15 * instance.size());
    xs = std::vector<double>(5 * instance.size());
  }

  void operator()() {
    reset();
    timers.push_back(Timer());
    timers.back().start();
    for (int i=0; i<instance.size()/2; ++i) {
      intrinsicsFilter(chi2 + i*2, cs.data() + i*2*15, xs.data() + i*2*5, instance[i*2], instance[i*2 + 1]);
    }
    timers.back().stop();
  }
};

#endif
