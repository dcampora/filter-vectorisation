#pragma once

#include <immintrin.h>
#include <emmintrin.h>
#include "Filter.h"

#if INSTRSET >= 7

__attribute__ ((noinline)) void intrinsicsFilter (double* chi2vector, double* c, double* x, FilterObject<double>& f0, FilterObject<double>& f1) {

  __m256d H01 = {f0.H[0], f1.H[0], f0.H[1], f1.H[1]};
  __m256d H23 = {f0.H[2], f1.H[2], f0.H[3], f1.H[3]};
  __m128d H4 = {f0.H[4], f1.H[4]};

  __m128d* H0 = (__m128d*) &H01;
  __m128d* H1 = H0 + 1;
  __m128d* H2 = (__m128d*) &H23;
  __m128d* H3 = H2 + 1;

  __m128d res = {f0.refResidual, f1.refResidual};
  __m128d errorRes2 = {f0.errorMeas2, f1.errorMeas2};

  __m128d CHT0;
  __m128d CHT1;
  __m128d CHT2;
  __m128d CHT3;
  __m128d CHT4;
  
  {
    __m256d C01 = {f0.Cprev[0], f1.Cprev[0], f0.Cprev[1], f1.Cprev[1]};
    __m256d C23 = {f0.Cprev[2], f1.Cprev[2], f0.Cprev[3], f1.Cprev[3]};
    __m256d C45 = {f0.Cprev[4], f1.Cprev[4], f0.Cprev[5], f1.Cprev[5]};
    __m256d C67 = {f0.Cprev[6], f1.Cprev[6], f0.Cprev[7], f1.Cprev[7]};
    __m256d C89 = {f0.Cprev[8], f1.Cprev[8], f0.Cprev[9], f1.Cprev[9]};
    __m256d C1011 = {f0.Cprev[10], f1.Cprev[10], f0.Cprev[11], f1.Cprev[11]};
    __m256d C1213 = {f0.Cprev[12], f1.Cprev[12], f0.Cprev[13], f1.Cprev[13]};
    __m128d C14 = {f0.Cprev[14], f1.Cprev[14]};

    __m128d* C0 = (__m128d*) &C01;
    __m128d* C1 = C0+1;
    __m128d* C2 = (__m128d*) &C23;
    __m128d* C3 = C2+1;
    __m128d* C4 = (__m128d*) &C45;
    __m128d* C5 = C4+1;
    __m128d* C6 = (__m128d*) &C67;
    __m128d* C7 = C6+1;
    __m128d* C8 = (__m128d*) &C89;
    __m128d* C9 = C8+1;
    __m128d* C10 = (__m128d*) &C1011;
    __m128d* C11 = C10+1;
    __m128d* C12 = (__m128d*) &C1213;
    __m128d* C13 = C12+1;

    CHT0 = _mm_add_pd(_mm_mul_pd(*C0, *H0),
                      _mm_add_pd(_mm_mul_pd(*C1, *H1),
                      _mm_add_pd(_mm_mul_pd(*C3, *H2),
                      _mm_add_pd(_mm_mul_pd(*C6, *H3),
                                  _mm_mul_pd(*C10, H4)))));

    // Const __m128d CHT1 = *C1*H0 + *C2*H1 + *C4*H2 + *C7*H3 + *C11H4;
    CHT1 = _mm_add_pd(_mm_mul_pd(*C1, *H0),
                      _mm_add_pd(_mm_mul_pd(*C2, *H1),
                      _mm_add_pd(_mm_mul_pd(*C4, *H2),
                      _mm_add_pd(_mm_mul_pd(*C7, *H3),
                                 _mm_mul_pd(*C11, H4)))));

    // Const __m128d CHT2 = *C3*H0 + *C4*H1 + *C5*H2 + *C8*H3 + *C12H4;
    CHT2 = _mm_add_pd(_mm_mul_pd(*C3, *H0),
                        _mm_add_pd(_mm_mul_pd(*C4, *H1),
                        _mm_add_pd(_mm_mul_pd(*C5, *H2),
                        _mm_add_pd(_mm_mul_pd(*C8, *H3),
                                    _mm_mul_pd(*C12, H4)))));

    // Const __m128d CHT3 = *C6*H0 + *C7*H1 + *C8*H2 + *C9*H3 + *C13H4;
    CHT3 = _mm_add_pd(_mm_mul_pd(*C6, *H0),
                        _mm_add_pd(_mm_mul_pd(*C7, *H1),
                        _mm_add_pd(_mm_mul_pd(*C8, *H2),
                        _mm_add_pd(_mm_mul_pd(*C9, *H3),
                                    _mm_mul_pd(*C13, H4)))));
    
    // Const __m128d CHT4 = *C10*H0 + *C11*H1 + *C12*H2 + *C13*H3 + *C14H4;
    CHT4 = _mm_add_pd(_mm_mul_pd(*C10, *H0),
                        _mm_add_pd(_mm_mul_pd(*C11, *H1),
                        _mm_add_pd(_mm_mul_pd(*C12, *H2),
                        _mm_add_pd(_mm_mul_pd(*C13, *H3),
                                    _mm_mul_pd(C14, H4)))));

    // errorRes2 += *H0CHT0 + *H1CHT1 + *H2CHT2 + *H3CHT3 + H4CHT4;
    errorRes2 = _mm_add_pd(errorRes2,
                _mm_add_pd(_mm_mul_pd(*H0, CHT0),
                _mm_add_pd(_mm_mul_pd(*H1, CHT1),
                _mm_add_pd(_mm_mul_pd(*H2, CHT2),
                _mm_add_pd(_mm_mul_pd(*H3, CHT3),
                            _mm_mul_pd(H4, CHT4))))));

    __m128d w1 = _mm_div_pd(_mm_set_pd1(1.), errorRes2);
    *C0  = _mm_sub_pd(*C0,  _mm_mul_pd(w1, _mm_mul_pd(CHT0, CHT0)));
    *C1  = _mm_sub_pd(*C1,  _mm_mul_pd(w1, _mm_mul_pd(CHT1, CHT0)));
    *C3  = _mm_sub_pd(*C3,  _mm_mul_pd(w1, _mm_mul_pd(CHT2, CHT0)));
    *C6  = _mm_sub_pd(*C6,  _mm_mul_pd(w1, _mm_mul_pd(CHT3, CHT0)));
    *C10 = _mm_sub_pd(*C10, _mm_mul_pd(w1, _mm_mul_pd(CHT4, CHT0)));
    *C2  = _mm_sub_pd(*C2,  _mm_mul_pd(w1, _mm_mul_pd(CHT1, CHT1)));
    *C4  = _mm_sub_pd(*C4,  _mm_mul_pd(w1, _mm_mul_pd(CHT2, CHT1)));
    *C7  = _mm_sub_pd(*C7,  _mm_mul_pd(w1, _mm_mul_pd(CHT3, CHT1)));
    *C11 = _mm_sub_pd(*C11, _mm_mul_pd(w1, _mm_mul_pd(CHT4, CHT1)));
    *C5  = _mm_sub_pd(*C5,  _mm_mul_pd(w1, _mm_mul_pd(CHT2, CHT2)));
    *C8  = _mm_sub_pd(*C8,  _mm_mul_pd(w1, _mm_mul_pd(CHT3, CHT2)));
    *C12 = _mm_sub_pd(*C12, _mm_mul_pd(w1, _mm_mul_pd(CHT4, CHT2)));
    *C9  = _mm_sub_pd(*C9,  _mm_mul_pd(w1, _mm_mul_pd(CHT3, CHT3)));
    *C13 = _mm_sub_pd(*C13, _mm_mul_pd(w1, _mm_mul_pd(CHT4, CHT3)));
    C14 = _mm_sub_pd(C14, _mm_mul_pd(w1, _mm_mul_pd(CHT4, CHT4)));

    // Update Cs
    _mm256_storeu_pd(c, C01);
    _mm256_storeu_pd(c+4, C23);
    _mm256_storeu_pd(c+8, C45);
    _mm256_storeu_pd(c+12, C67);
    _mm256_storeu_pd(c+16, C89);
    _mm256_storeu_pd(c+20, C1011);
    _mm256_storeu_pd(c+24, C1213);
    _mm_storeu_pd(c+28, C14);
  }

  {
    __m256d X01 = {f0.Xprev[0], f1.Xprev[0], f0.Xprev[1], f1.Xprev[1]};
    __m256d X23 = {f0.Xprev[2], f1.Xprev[2], f0.Xprev[3], f1.Xprev[3]};
    __m128d X4 = {f0.Xprev[4], f1.Xprev[4]};
    
    __m256d Xref01 = {f0.Xref[0], f1.Xref[0], f0.Xref[1], f1.Xref[1]};
    __m256d Xref23 = {f0.Xref[2], f1.Xref[2], f0.Xref[3], f1.Xref[3]};
    __m128d Xref4 = {f0.Xref[4], f1.Xref[4]};

    __m128d* X0 = (__m128d*) &X01;
    __m128d* X1 = X0+1;
    __m128d* X2 = (__m128d*) &X23;
    __m128d* X3 = X2+1;

    __m128d* Xref0 = (__m128d*) &Xref01;
    __m128d* Xref1 = Xref0+1;
    __m128d* Xref2 = (__m128d*) &Xref23;
    __m128d* Xref3 = Xref2+1;

    res = _mm_add_pd(res,
           _mm_add_pd(_mm_mul_pd(*H0, _mm_sub_pd(*Xref0, *X0)),
           _mm_add_pd(_mm_mul_pd(*H1, _mm_sub_pd(*Xref1, *X1)),
           _mm_add_pd(_mm_mul_pd(*H2, _mm_sub_pd(*Xref2, *X2)),
           _mm_add_pd(_mm_mul_pd(*H3, _mm_sub_pd(*Xref3, *X3)),
                      _mm_mul_pd(H4, _mm_sub_pd(Xref4, X4)))))));

    __m128d w0 = _mm_div_pd(res, errorRes2);
    *X0 = _mm_add_pd(*X0, _mm_mul_pd(CHT0, w0));
    *X1 = _mm_add_pd(*X1, _mm_mul_pd(CHT1, w0));
    *X2 = _mm_add_pd(*X2, _mm_mul_pd(CHT2, w0));
    *X3 = _mm_add_pd(*X3, _mm_mul_pd(CHT3, w0));
    X4 = _mm_add_pd(X4, _mm_mul_pd(CHT4, w0));

    _mm256_storeu_pd(x, X01);
    _mm256_storeu_pd(x+4, X23);
    _mm_storeu_pd(x+8, X4);
  }

  __m128d chi2 = _mm_div_pd(_mm_mul_pd(res, res), errorRes2);
  _mm_store_pd(chi2vector, chi2);
}

#endif

// This version requires 32 registers
void originalIntrinsicsFilter (double* chi2vector, double* c, double* x, FilterObject<double>& f0, FilterObject<double>& f1) {

  __m128d X0 = {f0.Xprev[0], f1.Xprev[0]};
  __m128d X1 = {f0.Xprev[1], f1.Xprev[1]};
  __m128d X2 = {f0.Xprev[2], f1.Xprev[2]};
  __m128d X3 = {f0.Xprev[3], f1.Xprev[3]};
  __m128d X4 = {f0.Xprev[4], f1.Xprev[4]};

  __m128d res = {f0.refResidual, f1.refResidual};
  __m128d errorRes2 = {f0.errorMeas2, f1.errorMeas2};

  const __m128d H0 = {f0.H[0], f1.H[0]};
  const __m128d H1 = {f0.H[1], f1.H[1]};
  const __m128d H2 = {f0.H[2], f1.H[2]};
  const __m128d H3 = {f0.H[3], f1.H[3]};
  const __m128d H4 = {f0.H[4], f1.H[4]};

  {
    const __m128d Xref0 = {f0.Xref[0], f1.Xref[0]};
    const __m128d Xref1 = {f0.Xref[1], f1.Xref[1]};
    const __m128d Xref2 = {f0.Xref[2], f1.Xref[2]};
    const __m128d Xref3 = {f0.Xref[3], f1.Xref[3]};
    const __m128d Xref4 = {f0.Xref[4], f1.Xref[4]};

    res = _mm_add_pd(res,
          _mm_add_pd(_mm_mul_pd(H0, _mm_sub_pd(Xref0, X0)),
          _mm_add_pd(_mm_mul_pd(H1, _mm_sub_pd(Xref1, X1)),
          _mm_add_pd(_mm_mul_pd(H2, _mm_sub_pd(Xref2, X2)),
          _mm_add_pd(_mm_mul_pd(H3, _mm_sub_pd(Xref3, X3)),
                     _mm_mul_pd(H4, _mm_sub_pd(Xref4, X4)))))));
  }

  __m128d C0 = {f0.Cprev[0], f1.Cprev[0]};
  __m128d C1 = {f0.Cprev[1], f1.Cprev[1]};
  __m128d C2 = {f0.Cprev[2], f1.Cprev[2]};
  __m128d C3 = {f0.Cprev[3], f1.Cprev[3]};
  __m128d C4 = {f0.Cprev[4], f1.Cprev[4]};
  __m128d C5 = {f0.Cprev[5], f1.Cprev[5]};
  __m128d C6 = {f0.Cprev[6], f1.Cprev[6]};
  __m128d C7 = {f0.Cprev[7], f1.Cprev[7]};
  __m128d C8 = {f0.Cprev[8], f1.Cprev[8]};
  __m128d C9 = {f0.Cprev[9], f1.Cprev[9]};
  __m128d C10 = {f0.Cprev[10], f1.Cprev[10]};
  __m128d C11 = {f0.Cprev[11], f1.Cprev[11]};
  __m128d C12 = {f0.Cprev[12], f1.Cprev[12]};
  __m128d C13 = {f0.Cprev[13], f1.Cprev[13]};
  __m128d C14 = {f0.Cprev[14], f1.Cprev[14]};

  const __m128d CHT0 = _mm_add_pd(_mm_mul_pd(C0, H0),
                      _mm_add_pd(_mm_mul_pd(C1, H1),
                      _mm_add_pd(_mm_mul_pd(C3, H2),
                      _mm_add_pd(_mm_mul_pd(C6, H3),
                                  _mm_mul_pd(C10, H4)))));

  // const __m128d CHT1 = C1*H0 + C2*H1 + C4*H2 + C7*H3 + C11*H4;
  const __m128d CHT1 = _mm_add_pd(_mm_mul_pd(C1, H0),
                      _mm_add_pd(_mm_mul_pd(C2, H1),
                      _mm_add_pd(_mm_mul_pd(C4, H2),
                      _mm_add_pd(_mm_mul_pd(C7, H3),
                                  _mm_mul_pd(C11, H4)))));

  // const __m128d CHT2 = C3*H0 + C4*H1 + C5*H2 + C8*H3 + C12*H4;
  const __m128d CHT2 = _mm_add_pd(_mm_mul_pd(C3, H0),
                      _mm_add_pd(_mm_mul_pd(C4, H1),
                      _mm_add_pd(_mm_mul_pd(C5, H2),
                      _mm_add_pd(_mm_mul_pd(C8, H3),
                                  _mm_mul_pd(C12, H4)))));

  // const __m128d CHT3 = C6*H0 + C7*H1 + C8*H2 + C9*H3 + C13*H4;
  const __m128d CHT3 = _mm_add_pd(_mm_mul_pd(C6, H0),
                      _mm_add_pd(_mm_mul_pd(C7, H1),
                      _mm_add_pd(_mm_mul_pd(C8, H2),
                      _mm_add_pd(_mm_mul_pd(C9, H3),
                                  _mm_mul_pd(C13, H4)))));
  
  // const __m128d CHT4 = C10*H0 + C11*H1 + C12*H2 + C13*H3 + C14*H4;
  const __m128d CHT4 = _mm_add_pd(_mm_mul_pd(C10, H0),
                      _mm_add_pd(_mm_mul_pd(C11, H1),
                      _mm_add_pd(_mm_mul_pd(C12, H2),
                      _mm_add_pd(_mm_mul_pd(C13, H3),
                                  _mm_mul_pd(C14, H4)))));

  // errorRes2 += H0*CHT0 + H1*CHT1 + H2*CHT2 + H3*CHT3 + H4*CHT4;
  errorRes2 = _mm_add_pd(errorRes2,
              _mm_add_pd(_mm_mul_pd(H0, CHT0),
              _mm_add_pd(_mm_mul_pd(H1, CHT1),
              _mm_add_pd(_mm_mul_pd(H2, CHT2),
              _mm_add_pd(_mm_mul_pd(H3, CHT3),
                         _mm_mul_pd(H4, CHT4))))));

  const __m128d w0 = _mm_div_pd(res, errorRes2);
  X0 = _mm_add_pd(X0, _mm_mul_pd(CHT0, w0));
  X1 = _mm_add_pd(X1, _mm_mul_pd(CHT1, w0));
  X2 = _mm_add_pd(X2, _mm_mul_pd(CHT2, w0));
  X3 = _mm_add_pd(X3, _mm_mul_pd(CHT3, w0));
  X4 = _mm_add_pd(X4, _mm_mul_pd(CHT4, w0));

  const __m128d w1 = _mm_div_pd(_mm_set_pd1(1.), errorRes2);
  C0  = _mm_sub_pd(C0,  _mm_mul_pd(w1, _mm_mul_pd(CHT0, CHT0)));
  C1  = _mm_sub_pd(C1,  _mm_mul_pd(w1, _mm_mul_pd(CHT1, CHT0)));
  C3  = _mm_sub_pd(C3,  _mm_mul_pd(w1, _mm_mul_pd(CHT2, CHT0)));
  C6  = _mm_sub_pd(C6,  _mm_mul_pd(w1, _mm_mul_pd(CHT3, CHT0)));
  C10 = _mm_sub_pd(C10, _mm_mul_pd(w1, _mm_mul_pd(CHT4, CHT0)));
  C2  = _mm_sub_pd(C2,  _mm_mul_pd(w1, _mm_mul_pd(CHT1, CHT1)));
  C4  = _mm_sub_pd(C4,  _mm_mul_pd(w1, _mm_mul_pd(CHT2, CHT1)));
  C7  = _mm_sub_pd(C7,  _mm_mul_pd(w1, _mm_mul_pd(CHT3, CHT1)));
  C11 = _mm_sub_pd(C11, _mm_mul_pd(w1, _mm_mul_pd(CHT4, CHT1)));
  C5  = _mm_sub_pd(C5,  _mm_mul_pd(w1, _mm_mul_pd(CHT2, CHT2)));
  C8  = _mm_sub_pd(C8,  _mm_mul_pd(w1, _mm_mul_pd(CHT3, CHT2)));
  C12 = _mm_sub_pd(C12, _mm_mul_pd(w1, _mm_mul_pd(CHT4, CHT2)));
  C9  = _mm_sub_pd(C9,  _mm_mul_pd(w1, _mm_mul_pd(CHT3, CHT3)));
  C13 = _mm_sub_pd(C13, _mm_mul_pd(w1, _mm_mul_pd(CHT4, CHT3)));
  C14 = _mm_sub_pd(C14, _mm_mul_pd(w1, _mm_mul_pd(CHT4, CHT4)));

  const __m128d chi2 = _mm_div_pd(_mm_mul_pd(res, res), errorRes2);

  _mm_storeu_pd(chi2vector, chi2);

  _mm_storeu_pd(c, C0);
  _mm_storeu_pd(c+2, C1);
  _mm_storeu_pd(c+4, C2);
  _mm_storeu_pd(c+6, C3);
  _mm_storeu_pd(c+8, C4);
  _mm_storeu_pd(c+10, C5);
  _mm_storeu_pd(c+12, C6);
  _mm_storeu_pd(c+14, C7);
  _mm_storeu_pd(c+16, C8);
  _mm_storeu_pd(c+18, C9);
  _mm_storeu_pd(c+20, C10);
  _mm_storeu_pd(c+22, C11);
  _mm_storeu_pd(c+24, C12);
  _mm_storeu_pd(c+26, C13);
  _mm_storeu_pd(c+28, C14);

  _mm_storeu_pd(x, X0);
  _mm_storeu_pd(x+2, X1);
  _mm_storeu_pd(x+4, X2);
  _mm_storeu_pd(x+6, X3);
  _mm_storeu_pd(x+8, X4);
}
