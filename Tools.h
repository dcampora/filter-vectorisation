#pragma once

#include <iostream>
#include <algorithm>
#include <fstream>
#include <vector>
#include <array>
#include <string>
#include <cmath>
#include "Timer.h"

class TimePrinter {
private:
  float baselineTime;

public:
  void printTimer(std::vector<Timer>& timers, const std::string& algName, bool taketime=false)
  {
    std::vector<float> counter (timers.size());
    std::for_each(timers.begin(), timers.end(), [&] (Timer& timers) { counter.push_back(timers.get()); });

    const double sum = std::accumulate(counter.begin(), counter.end(), 0.0);
    const double mean = sum / counter.size();
    const double sq_sum = std::inner_product(counter.begin(), counter.end(), counter.begin(), 0.0);
    const double stdev = std::sqrt(sq_sum / counter.size() - mean * mean);

    std::string speedup = "1.0x";
    if (taketime) baselineTime = sum;
    else          speedup = "" + std::to_string(baselineTime / sum) + "x";

    std::cout << algName << " mean: " << mean << " sum: " << sum << " stddev: " << stdev << " speedup: " << speedup << std::endl;
  }
};

void readBinary (const std::string& filename, long long int& size, std::vector<Filterd>& memblock) {
  std::ifstream file (filename, std::ios::in | std::ios::binary | std::ios::ate);
  if (file.is_open())
  {
    size = (long long int) file.tellg();
    memblock.resize(size / sizeof(Filterd));
    file.seekg (0, std::ios::beg);
    file.read ((char*) memblock.data(), size);
    file.close();
  }
  else std::cout << "Unable to open file";
}

template<class T>
void printArray (const std::string& name, const T& array) {
  std::cout << name << ": ";
  for (const auto& i : array) std::cout << i << ", ";
  std::cout << std::endl;
}

template<class P>
void printFilterObject (const FilterObject<P>& f, bool printall = true) {
  printArray("Xprev", f.Xprev);
  printArray("Cprev", f.Cprev);
  printArray("X", f.X);
  printArray("C", f.C);
  if (printall) {
    printArray("Xref", f.Xref);
    printArray("H", f.H);
    std::cout << "refResidual: " << f.refResidual << std::endl;
    std::cout << "errorMeas2: " << f.errorMeas2 << std::endl;
  }
  std::cout << "chi2: " << f.chi2 << std::endl;
}

template<class P0, class P1>
bool checkResult (
  const FilterObject<P0>& f0,
  const FilterObject<P1>& f1,
  const P0 f0chi2,
  const P1 f1chi2,
  std::vector<double>& maxEpsilons,
  const bool checkOnlyChi2 = false,
  const double acceptable_epsilon = 1.e-7
  ){
  // Check result and chi2 match what they should be
  bool xequal=true, cequal=true, chi2equal=true;
  if (!checkOnlyChi2) {
    for (int i=0; i<5; ++i) {
      xequal &= std::abs(f0.X[i] - f1.X[i]) < acceptable_epsilon;
      maxEpsilons[0] = std::max(maxEpsilons[0], std::abs(f0.X[i] - f1.X[i]));
    }
    for (int i=0; i<15; ++i) {
      cequal &= std::abs(f0.C[i] - f1.C[i]) < acceptable_epsilon;
      maxEpsilons[1] = std::max(maxEpsilons[1], std::abs(f0.C[i] - f1.C[i]));
    }
  }
  chi2equal = std::abs(f0chi2 - f1chi2) < acceptable_epsilon;
  maxEpsilons[2] = std::max(maxEpsilons[2], std::abs(f0chi2 - f1chi2));

  return chi2equal && xequal && cequal;
}

template<class P0, class P1>
void printMaxEpsilon(
  FilterObject<P0>& seq,
  FilterObject<P1>& input,
  const double seqChi2,
  const double vecChi2) {

  double xepsilon=0.0, cepsilon=0.0, chi2eps=0.0;
  for (int i=0; i<5; ++i) xepsilon = std::max(xepsilon, std::abs((double) seq.X[i] - input.X[i]));
  for (int i=0; i<5; ++i) cepsilon = std::max(cepsilon, std::abs((double) seq.C[i] - input.C[i]));
  chi2eps = std::max(chi2eps, std::abs((double) seqChi2 - vecChi2));

  std::cout << "seq and vec epsilons (x, c, chi2): (" << xepsilon << ", " << cepsilon << ", " << chi2eps << ")";
}
