#pragma once

#include "Filter.h"

// Update filtered vector objects recursively
template<class T, class R>
void updateFilteredObject (
  int index,
  const T& X,
  R& filteredX
  ){

  for (int i=0; i<X.size(); ++i) {
    filteredX[i] = X[i][index];
  }
}
template<class T, class R, class ... Rargs>
void updateFilteredObject (
  int index,
  const T& X,
  R& filteredX,
  Rargs& ... Fargs
  ){

  for (int i=0; i<X.size(); ++i) {
    filteredX[i] = X[i][index];
  }
  updateFilteredObject(++index, X, Fargs...);
}

// AVX 512
#if INSTRSET >= 9

// Type traits for vector width
template <typename T>
struct Vec4 {
};
template<>
struct Vec4<float> {
  typedef Vec4f type;
};
template<>
struct Vec4<double> {
  typedef Vec4d type;
};
template <typename T>
struct Vec8 {
};
template<>
struct Vec8<float> {
  typedef Vec8f type;
};
template<>
struct Vec8<double> {
  typedef Vec8d type;
};

template<typename T>
typename Vec4<T>::type make_vector (T t1, T t2, T t3, T t4=0) {
  return typename Vec4<T>::type(t1,t2,t3,t4);
}
template<typename T>
typename Vec8<T>::type make_vector (T t1, T t2, T t3, T t4, T t5, T t6=0, T t7=0, T t8=0) {
  return typename Vec8<T>::type(t1,t2,t3,t4,t5,t6,t7,t8);
}
template<typename T>
Vec16f make_vector(T t1, T t2, T t3, T t4, T t5, T t6, T t7, T t8, T t9, T t10=0, T t11=0, T t12=0, T t13=0, T t14=0, T t15=0, T t16=0) {
  return Vec16f(t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13,t14,t15,t16);
}

#elif INSTRSET >= 7

template <typename T>
struct Vec4 {
};
template<>
struct Vec4<float> {
  typedef Vec4f type;
};
template<>
struct Vec4<double> {
  typedef Vec4d type;
};

template<typename T>
inline typename Vec4<T>::type make_vector (T t1, T t2, T t3, T t4=0) {
  return typename Vec4<T>::type(t1,t2,t3,t4);
}
template<typename T>
Vec8f make_vector (T t1, T t2, T t3, T t4, T t5, T t6=0, T t7=0, T t8=0) {
  return Vec8f(t1,t2,t3,t4,t5,t6,t7,t8);
}

#else

template<typename T>
Vec4f make_vector (T t1, T t2, T t3, T t4=0) {
  return Vec4f(t1,t2,t3,t4);
}

#endif

template<typename T>
inline Vec2d make_vector(T t1, T t2) {
  return Vec2d(t1,t2);
}
template<typename T>
T make_vector(T t1) {
  return t1;
}

// Invokes the filter over n elements with precision P
// Note: For a scalar version, the filter function alone is more efficient
//       and hence this function does not support scalars. If needed, it could with a
//       simple partial specialization of updateFilteredObject recursive method.
template<class P, class ... FilterObject>
__attribute__ ((noinline)) void invokeFilter (P* chi2vector, FilterObject&... filterobjects) {
  using T = decltype(make_vector<P>(filterobjects.Xprev[0]...));

  std::array<T, 5> X =
    {make_vector<P>(filterobjects.Xprev[0]...),
    make_vector<P>(filterobjects.Xprev[1]...),
    make_vector<P>(filterobjects.Xprev[2]...),
    make_vector<P>(filterobjects.Xprev[3]...),
    make_vector<P>(filterobjects.Xprev[4]...)
    };

  std::array<T, 15> C =
    {make_vector<P>(filterobjects.Cprev[0]...),
    make_vector<P>(filterobjects.Cprev[1]...),
    make_vector<P>(filterobjects.Cprev[2]...),
    make_vector<P>(filterobjects.Cprev[3]...),
    make_vector<P>(filterobjects.Cprev[4]...),
    make_vector<P>(filterobjects.Cprev[5]...),
    make_vector<P>(filterobjects.Cprev[6]...),
    make_vector<P>(filterobjects.Cprev[7]...),
    make_vector<P>(filterobjects.Cprev[8]...),
    make_vector<P>(filterobjects.Cprev[9]...),
    make_vector<P>(filterobjects.Cprev[10]...),
    make_vector<P>(filterobjects.Cprev[11]...),
    make_vector<P>(filterobjects.Cprev[12]...),
    make_vector<P>(filterobjects.Cprev[13]...),
    make_vector<P>(filterobjects.Cprev[14]...)
    };

  std::array<T, 5> Xref =
    {make_vector<P>(filterobjects.Xref[0]...),
    make_vector<P>(filterobjects.Xref[1]...),
    make_vector<P>(filterobjects.Xref[2]...),
    make_vector<P>(filterobjects.Xref[3]...),
    make_vector<P>(filterobjects.Xref[4]...)
    };

  std::array<T, 5> H =
    {make_vector<P>(filterobjects.H[0]...),
    make_vector<P>(filterobjects.H[1]...),
    make_vector<P>(filterobjects.H[2]...),
    make_vector<P>(filterobjects.H[3]...),
    make_vector<P>(filterobjects.H[4]...)
    };

  T vRefResidual = make_vector<P>(filterobjects.refResidual...);
  T verrorMeas2 = make_vector<P>(filterobjects.errorMeas2...);

  T vchi2 = templatedFilter(X, C, Xref, H, vRefResidual, verrorMeas2);

  vchi2.store(chi2vector);
  updateFilteredObject(0, X, filterobjects.Xprev...);
  updateFilteredObject(0, C, filterobjects.Cprev...);
}
