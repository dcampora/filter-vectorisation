#pragma once

#include "VectorGenericFilter.h"

template<int N0, int N1, int N2, int N3>
inline Vec4d blend4 (Vec4d const & t0, Vec4d const & t1) {
  return blend4d<N0,N1,N2,N3>(t0, t1);
}
template<int N0, int N1, int N2, int N3>
inline Vec4f blend4 (Vec4f const & t0, Vec4f const & t1) {
  return blend4f<N0,N1,N2,N3>(t0, t1);
}

template<class T>
inline void load_soa (T& v0, T& v1, T& temp, const double* a0, const double* a1) {
  temp.load(a0);
  v1.load(a1);
  v0 = blend2d<0, 2>(temp, v1);
  v1 = blend2d<1, 3>(temp, v1);
}

template<class T>
inline void load_soa (T& v0, T& v1, T& v2, T& v3, T& temp, const double* a0, const double* a1, const double* a2, const double* a3) {
  temp.load(a0);
  v1.load(a1);
  v2.load(a2);
  v3.load(a3);

  // Fill in v0
  v0 = blend4<0, 4, -256, -256>(temp, v1);
  v0 = blend4<0, 1, 4, -256>(v0, v2);
  v0 = blend4<0, 1, 2, 4>(v0, v3);

  // v1
  temp = blend4<5, 1, 2, 3>(temp, v2);
  v1   = blend4<5, 1, 2, 3>(v1, v3);
  v2   = blend4<6, 7, 2, 3>(v2, v1);
  v1   = blend4<1, 5, 0, 4>(temp, v1);

  // v3
  v3 = blend4<6, 1, 3, 7>(v2, v3);
  temp = blend4<0, -256, 6, 7>(v3, temp);
  v3 = blend4<3, 5, 6, 7>(temp, v3);

  // v2
  v2 = blend4<2, 4, 6, 0>(temp, v2);
}

template<class P, class ... D>
inline auto load_array_5 (D&... A) -> std::array<decltype(make_vector<P>(A[0]...)), 5> {
  using T = decltype(make_vector<P>(A[0]...));

  return std::array<T, 5>(
    {make_vector<P>(A[0]...),
    make_vector<P>(A[1]...),
    make_vector<P>(A[2]...),
    make_vector<P>(A[3]...),
    make_vector<P>(A[4]...)
    });
}

// Optimised versions to load elements
template<class P>
inline auto load_array_5 (P (&x0) [5], P (&x1) [5]) -> std::array<decltype(make_vector<P>(x0[0], x1[0])), 5> {
  using T = decltype(make_vector<P>(x0[0], x1[0]));
  std::array<T, 5> v;

  // Use fifth element as temp
  // Loads first two elements of both and interchange them
  load_soa(v[0], v[1], v[4], &x0[0], &x1[0]);
  load_soa(v[2], v[3], v[4], &x0[2], &x1[2]);

  // Finally load fifth element and return
  v[4] = T(x0[4], x1[4]);

  return v;
}

template<class P, class ... D>
inline auto load_array_15 (D&... A) -> std::array<decltype(make_vector<P>(A[0]...)), 15> {
  using T = decltype(make_vector<P>(A[0]...));

  return std::array<T, 15>(
    {make_vector<P>(A[0]...),
    make_vector<P>(A[1]...),
    make_vector<P>(A[2]...),
    make_vector<P>(A[3]...),
    make_vector<P>(A[4]...),
    make_vector<P>(A[5]...),
    make_vector<P>(A[6]...),
    make_vector<P>(A[7]...),
    make_vector<P>(A[8]...),
    make_vector<P>(A[9]...),
    make_vector<P>(A[10]...),
    make_vector<P>(A[11]...),
    make_vector<P>(A[12]...),
    make_vector<P>(A[13]...),
    make_vector<P>(A[4]...)
    });
}

// Optimised versions
template<class P>
inline auto load_array_15 (P x0 [15], P x1 [15]) -> std::array<decltype(make_vector<P>(x0[0], x1[0])), 15> {
  using T = decltype(make_vector<P>(x0[0], x1[0]));
  std::array<T, 15> v;

  // Use fifth element as temp
  // Loads first two elements of both and interchange them
  for (int i=0; i<15/2; ++i) {
    load_soa(v[i*2], v[i*2+1], v[14], &x0[i*2], &x1[i*2]);
  }

  // Finally load fifth element and return
  v[14] = T(x0[14], x1[14]);

  return v;
}

template<class P>
inline auto load_array_15 (P x0 [15], P x1 [15], P x2 [15], P x3 [15]) -> std::array<decltype(make_vector<P>(x0[0], x1[0], x2[0], x3[0])), 15> {
  using T = decltype(make_vector<P>(x0[0], x1[0], x2[0], x3[0]));
  std::array<T, 15> v;

  // Use fifth element as temp
  // Loads first two elements of both and interchange them
  for (int i=0; i<15/4; ++i) {
    load_soa(v[i*4], v[i*4+1], v[i*4+2], v[i*4+3], v[14], &x0[i*2], &x1[i*2], &x2[i*2], &x3[i*2]);
  }

  // Finally load fifth element and return
  v[14] = T(x0[14], x1[14]);

  return v;
}

template<class P, class ... F>
__attribute__ ((noinline)) void optimisedFilter (P* chi2vector, F&... filterobjects) {
  using T = decltype(make_vector<P>(filterobjects.Xprev[0]...));

  std::array<T, 5> X (
    {make_vector<P>(filterobjects.Xprev[0]...),
    make_vector<P>(filterobjects.Xprev[1]...),
    make_vector<P>(filterobjects.Xprev[2]...),
    make_vector<P>(filterobjects.Xprev[3]...),
    make_vector<P>(filterobjects.Xprev[4]...)
    });

  std::array<T, 15> C (
    {make_vector<P>(filterobjects.Cprev[0]...),
    make_vector<P>(filterobjects.Cprev[1]...),
    make_vector<P>(filterobjects.Cprev[2]...),
    make_vector<P>(filterobjects.Cprev[3]...),
    make_vector<P>(filterobjects.Cprev[4]...),
    make_vector<P>(filterobjects.Cprev[5]...),
    make_vector<P>(filterobjects.Cprev[6]...),
    make_vector<P>(filterobjects.Cprev[7]...),
    make_vector<P>(filterobjects.Cprev[8]...),
    make_vector<P>(filterobjects.Cprev[9]...),
    make_vector<P>(filterobjects.Cprev[10]...),
    make_vector<P>(filterobjects.Cprev[11]...),
    make_vector<P>(filterobjects.Cprev[12]...),
    make_vector<P>(filterobjects.Cprev[13]...),
    make_vector<P>(filterobjects.Cprev[4]...)
    });

  std::array<T, 5> CHT; 

  T res = make_vector<P>(filterobjects.refResidual...);
  T errorRes2 = make_vector<P>(filterobjects.errorMeas2...);

  {
    const std::array<T, 5> H (
    {make_vector<P>(filterobjects.H[0]...),
    make_vector<P>(filterobjects.H[1]...),
    make_vector<P>(filterobjects.H[2]...),
    make_vector<P>(filterobjects.H[3]...),
    make_vector<P>(filterobjects.H[4]...)
    });

    {
      const std::array<T, 5> Xref (
    {make_vector<P>(filterobjects.Xref[0]...),
    make_vector<P>(filterobjects.Xref[1]...),
    make_vector<P>(filterobjects.Xref[2]...),
    make_vector<P>(filterobjects.Xref[3]...),
    make_vector<P>(filterobjects.Xref[4]...)
    });
      res += H[0] * (Xref[0] - X[0]) 
          +  H[1] * (Xref[1] - X[1])
          +  H[2] * (Xref[2] - X[2])
          +  H[3] * (Xref[3] - X[3]) 
          +  H[4] * (Xref[4] - X[4]);
    }

    CHT = {
     C[ 0]*H[0] + C[ 1]*H[1] + C[ 3]*H[2] + C[ 6]*H[3] + C[10]*H[4],
     C[ 1]*H[0] + C[ 2]*H[1] + C[ 4]*H[2] + C[ 7]*H[3] + C[11]*H[4],
     C[ 3]*H[0] + C[ 4]*H[1] + C[ 5]*H[2] + C[ 8]*H[3] + C[12]*H[4],
     C[ 6]*H[0] + C[ 7]*H[1] + C[ 8]*H[2] + C[ 9]*H[3] + C[13]*H[4],
     C[10]*H[0] + C[11]*H[1] + C[12]*H[2] + C[13]*H[3] + C[14]*H[4] 
    };

    errorRes2 += H[0]*CHT[0] 
               + H[1]*CHT[1] 
               + H[2]*CHT[2]
               + H[3]*CHT[3] 
               + H[4]*CHT[4];
  }

  const T w0 = res/errorRes2;
  X[0] += CHT[0] * w0;
  X[1] += CHT[1] * w0;
  X[2] += CHT[2] * w0;
  X[3] += CHT[3] * w0;
  X[4] += CHT[4] * w0;

  const T w1 = 1./errorRes2;
  C[ 0] -= w1 * CHT[0] * CHT[0];
  C[ 1] -= w1 * CHT[1] * CHT[0];
  C[ 3] -= w1 * CHT[2] * CHT[0];
  C[ 6] -= w1 * CHT[3] * CHT[0];
  C[10] -= w1 * CHT[4] * CHT[0];
  C[ 2] -= w1 * CHT[1] * CHT[1];
  C[ 4] -= w1 * CHT[2] * CHT[1];
  C[ 7] -= w1 * CHT[3] * CHT[1];
  C[11] -= w1 * CHT[4] * CHT[1];
  C[ 5] -= w1 * CHT[2] * CHT[2];
  C[ 8] -= w1 * CHT[3] * CHT[2];
  C[12] -= w1 * CHT[4] * CHT[2];
  C[ 9] -= w1 * CHT[3] * CHT[3];
  C[13] -= w1 * CHT[4] * CHT[3];
  C[14] -= w1 * CHT[4] * CHT[4];

  const T chi2 = res*res / errorRes2;

  // Store our calculation back
  chi2.store(chi2vector);
  updateFilteredObject(0, X, filterobjects.X...);
  updateFilteredObject(0, C, filterobjects.C...);
}

// This version requires 32 registers
template<class P, class ... FilterObject>
__attribute__ ((noinline)) void moreOptimisedFilter (P* chi2vector, FilterObject&... filterobjects) {
  using T = decltype(make_vector<P>(filterobjects.Xprev[0]...));

  T res = make_vector<P>(filterobjects.refResidual...);
  T errorRes2 = make_vector<P>(filterobjects.errorMeas2...);

  std::array<T, 5> CHT;
    const std::array<T, 5> H (
    {make_vector<P>(filterobjects.H[0]...),
    make_vector<P>(filterobjects.H[1]...),
    make_vector<P>(filterobjects.H[2]...),
    make_vector<P>(filterobjects.H[3]...),
    make_vector<P>(filterobjects.H[4]...)
    });

  {
    // Calculate new covariance
  std::array<T, 15> C (
    {make_vector<P>(filterobjects.Cprev[0]...),
    make_vector<P>(filterobjects.Cprev[1]...),
    make_vector<P>(filterobjects.Cprev[2]...),
    make_vector<P>(filterobjects.Cprev[3]...),
    make_vector<P>(filterobjects.Cprev[4]...),
    make_vector<P>(filterobjects.Cprev[5]...),
    make_vector<P>(filterobjects.Cprev[6]...),
    make_vector<P>(filterobjects.Cprev[7]...),
    make_vector<P>(filterobjects.Cprev[8]...),
    make_vector<P>(filterobjects.Cprev[9]...),
    make_vector<P>(filterobjects.Cprev[10]...),
    make_vector<P>(filterobjects.Cprev[11]...),
    make_vector<P>(filterobjects.Cprev[12]...),
    make_vector<P>(filterobjects.Cprev[13]...),
    make_vector<P>(filterobjects.Cprev[4]...)
    });

    CHT = {
     C[ 0]*H[0] + C[ 1]*H[1] + C[ 3]*H[2] + C[ 6]*H[3] + C[10]*H[4],
     C[ 1]*H[0] + C[ 2]*H[1] + C[ 4]*H[2] + C[ 7]*H[3] + C[11]*H[4],
     C[ 3]*H[0] + C[ 4]*H[1] + C[ 5]*H[2] + C[ 8]*H[3] + C[12]*H[4],
     C[ 6]*H[0] + C[ 7]*H[1] + C[ 8]*H[2] + C[ 9]*H[3] + C[13]*H[4],
     C[10]*H[0] + C[11]*H[1] + C[12]*H[2] + C[13]*H[3] + C[14]*H[4] 
    };

    errorRes2 += H[0]*CHT[0] 
               + H[1]*CHT[1] 
               + H[2]*CHT[2]
               + H[3]*CHT[3] 
               + H[4]*CHT[4];
    
    const T w1 = 1./errorRes2;
    C[ 0] -= w1 * CHT[0] * CHT[0];
    C[ 1] -= w1 * CHT[1] * CHT[0];
    C[ 3] -= w1 * CHT[2] * CHT[0];
    C[ 6] -= w1 * CHT[3] * CHT[0];
    C[10] -= w1 * CHT[4] * CHT[0];
    C[ 2] -= w1 * CHT[1] * CHT[1];
    C[ 4] -= w1 * CHT[2] * CHT[1];
    C[ 7] -= w1 * CHT[3] * CHT[1];
    C[11] -= w1 * CHT[4] * CHT[1];
    C[ 5] -= w1 * CHT[2] * CHT[2];
    C[ 8] -= w1 * CHT[3] * CHT[2];
    C[12] -= w1 * CHT[4] * CHT[2];
    C[ 9] -= w1 * CHT[3] * CHT[3];
    C[13] -= w1 * CHT[4] * CHT[3];
    C[14] -= w1 * CHT[4] * CHT[4];

    updateFilteredObject(0, C, filterobjects.C...);
  }

  {
    // Calculate X
  std::array<T, 5> X (
    {make_vector<P>(filterobjects.Xprev[0]...),
    make_vector<P>(filterobjects.Xprev[1]...),
    make_vector<P>(filterobjects.Xprev[2]...),
    make_vector<P>(filterobjects.Xprev[3]...),
    make_vector<P>(filterobjects.Xprev[4]...)
    });

    {
      const std::array<T, 5> Xref (
    {make_vector<P>(filterobjects.Xref[0]...),
    make_vector<P>(filterobjects.Xref[1]...),
    make_vector<P>(filterobjects.Xref[2]...),
    make_vector<P>(filterobjects.Xref[3]...),
    make_vector<P>(filterobjects.Xref[4]...)
    });
      res += H[0] * (Xref[0] - X[0]) 
          +  H[1] * (Xref[1] - X[1])
          +  H[2] * (Xref[2] - X[2])
          +  H[3] * (Xref[3] - X[3]) 
          +  H[4] * (Xref[4] - X[4]);
    }

    const T w0 = res/errorRes2;
    X[0] += CHT[0] * w0;
    X[1] += CHT[1] * w0;
    X[2] += CHT[2] * w0;
    X[3] += CHT[3] * w0;
    X[4] += CHT[4] * w0;

    updateFilteredObject(0, X, filterobjects.X...);
  }
  
  const T chi2 = res*res / errorRes2;
  chi2.store(chi2vector);
}

// This version requires 32 registers
template<class P, class ... FilterObject>
__attribute__ ((noinline)) auto restrictedFilterArray2 (P* chi2vector, FilterObject&... filterobjects) -> decltype(make_vector<P>(filterobjects.Xprev[0]...)) {
  using T = decltype(make_vector<P>(filterobjects.Xprev[0]...));

  std::array<T, 5> X =
    {make_vector<P>(filterobjects.Xprev[0]...),
    make_vector<P>(filterobjects.Xprev[1]...),
    make_vector<P>(filterobjects.Xprev[2]...),
    make_vector<P>(filterobjects.Xprev[3]...),
    make_vector<P>(filterobjects.Xprev[4]...)
    };

  T res = make_vector<P>(filterobjects.refResidual...);
  T errorRes2 = make_vector<P>(filterobjects.errorMeas2...);

  const std::array<T, 5> H =
    {make_vector<P>(filterobjects.H[0]...),
    make_vector<P>(filterobjects.H[1]...),
    make_vector<P>(filterobjects.H[2]...),
    make_vector<P>(filterobjects.H[3]...),
    make_vector<P>(filterobjects.H[4]...)
    };

  {
    const std::array<T, 5> Xref =
      {make_vector<P>(filterobjects.Xref[0]...),
      make_vector<P>(filterobjects.Xref[1]...),
      make_vector<P>(filterobjects.Xref[2]...),
      make_vector<P>(filterobjects.Xref[3]...),
      make_vector<P>(filterobjects.Xref[4]...)
      };

    res += H[0] * (Xref[0] - X[0]) 
        +  H[1] * (Xref[1] - X[1])
        +  H[2] * (Xref[2] - X[2])
        +  H[3] * (Xref[3] - X[3]) 
        +  H[4] * (Xref[4] - X[4]);
  }

  std::array<T, 15> C =
    {make_vector<P>(filterobjects.Cprev[0]...),
    make_vector<P>(filterobjects.Cprev[1]...),
    make_vector<P>(filterobjects.Cprev[2]...),
    make_vector<P>(filterobjects.Cprev[3]...),
    make_vector<P>(filterobjects.Cprev[4]...),
    make_vector<P>(filterobjects.Cprev[5]...),
    make_vector<P>(filterobjects.Cprev[6]...),
    make_vector<P>(filterobjects.Cprev[7]...),
    make_vector<P>(filterobjects.Cprev[8]...),
    make_vector<P>(filterobjects.Cprev[9]...),
    make_vector<P>(filterobjects.Cprev[10]...),
    make_vector<P>(filterobjects.Cprev[11]...),
    make_vector<P>(filterobjects.Cprev[12]...),
    make_vector<P>(filterobjects.Cprev[13]...),
    make_vector<P>(filterobjects.Cprev[14]...)
    };

  const std::array<T, 5> CHT = {
   C[ 0]*H[0] + C[ 1]*H[1] + C[ 3]*H[2] + C[ 6]*H[3] + C[10]*H[4],
   C[ 1]*H[0] + C[ 2]*H[1] + C[ 4]*H[2] + C[ 7]*H[3] + C[11]*H[4],
   C[ 3]*H[0] + C[ 4]*H[1] + C[ 5]*H[2] + C[ 8]*H[3] + C[12]*H[4],
   C[ 6]*H[0] + C[ 7]*H[1] + C[ 8]*H[2] + C[ 9]*H[3] + C[13]*H[4],
   C[10]*H[0] + C[11]*H[1] + C[12]*H[2] + C[13]*H[3] + C[14]*H[4] 
  };

  errorRes2 += H[0]*CHT[0] 
             + H[1]*CHT[1] 
             + H[2]*CHT[2]
             + H[3]*CHT[3] 
             + H[4]*CHT[4];

  const T w0 = res/errorRes2;
  X[0] += CHT[0] * w0;
  X[1] += CHT[1] * w0;
  X[2] += CHT[2] * w0;
  X[3] += CHT[3] * w0;
  X[4] += CHT[4] * w0;

  const T w1 = 1./errorRes2;
  C[ 0] -= w1 * CHT[0] * CHT[0];
  C[ 1] -= w1 * CHT[1] * CHT[0];
  C[ 3] -= w1 * CHT[2] * CHT[0];
  C[ 6] -= w1 * CHT[3] * CHT[0];
  C[10] -= w1 * CHT[4] * CHT[0];
  C[ 2] -= w1 * CHT[1] * CHT[1];
  C[ 4] -= w1 * CHT[2] * CHT[1];
  C[ 7] -= w1 * CHT[3] * CHT[1];
  C[11] -= w1 * CHT[4] * CHT[1];
  C[ 5] -= w1 * CHT[2] * CHT[2];
  C[ 8] -= w1 * CHT[3] * CHT[2];
  C[12] -= w1 * CHT[4] * CHT[2];
  C[ 9] -= w1 * CHT[3] * CHT[3];
  C[13] -= w1 * CHT[4] * CHT[3];
  C[14] -= w1 * CHT[4] * CHT[4];

  const T chi2 = res*res / errorRes2;

  // Store our calculation back
  chi2.store(chi2vector);
  updateFilteredObject(0, X, filterobjects.X...);
  updateFilteredObject(0, C, filterobjects.C...);
}

// This version requires 32 registers
template<class P, class ... FilterObject>
__attribute__ ((noinline)) auto restrictedFilter (FilterObject&... filterobjects) -> decltype(make_vector<P>(filterobjects.Xprev[0]...)) {
  using T = decltype(make_vector<P>(filterobjects.Xprev[0]...));

  T X0 = make_vector<P>(filterobjects.X[0]...);
  T X1 = make_vector<P>(filterobjects.X[1]...);
  T X2 = make_vector<P>(filterobjects.X[2]...);
  T X3 = make_vector<P>(filterobjects.X[3]...);
  T X4 = make_vector<P>(filterobjects.X[4]...);

  T C0 = make_vector<P>(filterobjects.C[0]...);
  T C1 = make_vector<P>(filterobjects.C[1]...);
  T C2 = make_vector<P>(filterobjects.C[2]...);
  T C3 = make_vector<P>(filterobjects.C[3]...);
  T C4 = make_vector<P>(filterobjects.C[4]...);
  T C5 = make_vector<P>(filterobjects.C[5]...);
  T C6 = make_vector<P>(filterobjects.C[6]...);
  T C7 = make_vector<P>(filterobjects.C[7]...);
  T C8 = make_vector<P>(filterobjects.C[8]...);
  T C9 = make_vector<P>(filterobjects.C[9]...);
  T C10 = make_vector<P>(filterobjects.C[10]...);
  T C11 = make_vector<P>(filterobjects.C[11]...);
  T C12 = make_vector<P>(filterobjects.C[12]...);
  T C13 = make_vector<P>(filterobjects.C[13]...);
  T C14 = make_vector<P>(filterobjects.C[14]...);

  T CHT0;
  T CHT1;
  T CHT2;
  T CHT3;
  T CHT4;

  T res = make_vector<P>(filterobjects.refResidual...);
  T errorRes2 = make_vector<P>(filterobjects.errorMeas2...);

  {
    const T H0 = make_vector<P>(filterobjects.H[0]...);
    const T H1 = make_vector<P>(filterobjects.H[1]...);
    const T H2 = make_vector<P>(filterobjects.H[2]...);
    const T H3 = make_vector<P>(filterobjects.H[3]...);
    const T H4 = make_vector<P>(filterobjects.H[4]...);

    {
      const T Xref0 = make_vector<P>(filterobjects.Xprev[0]...);
      const T Xref1 = make_vector<P>(filterobjects.Xprev[1]...);
      const T Xref2 = make_vector<P>(filterobjects.Xprev[2]...);
      const T Xref3 = make_vector<P>(filterobjects.Xprev[3]...);
      const T Xref4 = make_vector<P>(filterobjects.Xprev[4]...);

      res += H0 * (Xref0 - X0) 
          +  H1 * (Xref1 - X1)
          +  H2 * (Xref2 - X2)
          +  H3 * (Xref3 - X3) 
          +  H4 * (Xref4 - X4);
    }

    CHT0 = C0*H0 + C1*H1 + C3*H2 + C6*H3 + C10*H4;
    CHT1 = C1*H0 + C2*H1 + C4*H2 + C7*H3 + C11*H4;
    CHT2 = C3*H0 + C4*H1 + C5*H2 + C8*H3 + C12*H4;
    CHT3 = C6*H0 + C7*H1 + C8*H2 + C9*H3 + C13*H4;
    CHT4 = C10*H0 + C11*H1 + C12*H2 + C13*H3 + C14*H4;
    errorRes2 += H0*CHT0 + H1*CHT1 + H2*CHT2 + H3*CHT3 + H4*CHT4;
  }

  const T w0 = res/errorRes2;
  X0 += CHT0 * w0;
  X1 += CHT1 * w0;
  X2 += CHT2 * w0;
  X3 += CHT3 * w0;
  X4 += CHT4 * w0;

  const T w1 = 1./errorRes2;
  C0 -= w1 * CHT0 * CHT0;
  C1 -= w1 * CHT1 * CHT0;
  C3 -= w1 * CHT2 * CHT0;
  C6 -= w1 * CHT3 * CHT0;
  C10 -= w1 * CHT4 * CHT0;

  C2 -= w1 * CHT1 * CHT1;
  C4 -= w1 * CHT2 * CHT1;
  C7 -= w1 * CHT3 * CHT1;
  C11 -= w1 * CHT4 * CHT1;

  C5 -= w1 * CHT2 * CHT2;
  C8 -= w1 * CHT3 * CHT2;
  C12 -= w1 * CHT4 * CHT2;

  C9 -= w1 * CHT3 * CHT3;
  C13 -= w1 * CHT4 * CHT3;

  C14 -= w1 * CHT4 * CHT4;

  const T chi2 = res*res / errorRes2;

  return chi2;
}


// This version requires 32 registers
template<class P, class ... FilterObject>
__attribute__ ((noinline)) void restrictedFilter2 (FilterObject&... filterobjects) {
  using T = decltype(make_vector<P>(filterobjects.Xprev[0]...));

  T X0 = make_vector<P>(filterobjects.X[0]...);
  T X1 = make_vector<P>(filterobjects.X[1]...);
  T X2 = make_vector<P>(filterobjects.X[2]...);
  T X3 = make_vector<P>(filterobjects.X[3]...);
  T X4 = make_vector<P>(filterobjects.X[4]...);

  T res = make_vector<P>(filterobjects.refResidual...);
  T errorRes2 = make_vector<P>(filterobjects.errorMeas2...);

  const T H0 = make_vector<P>(filterobjects.H[0]...);
  const T H1 = make_vector<P>(filterobjects.H[1]...);
  const T H2 = make_vector<P>(filterobjects.H[2]...);
  const T H3 = make_vector<P>(filterobjects.H[3]...);
  const T H4 = make_vector<P>(filterobjects.H[4]...);

  {
    const T Xref0 = make_vector<P>(filterobjects.Xprev[0]...);
    const T Xref1 = make_vector<P>(filterobjects.Xprev[1]...);
    const T Xref2 = make_vector<P>(filterobjects.Xprev[2]...);
    const T Xref3 = make_vector<P>(filterobjects.Xprev[3]...);
    const T Xref4 = make_vector<P>(filterobjects.Xprev[4]...);

    res += H0 * (Xref0 - X0) 
        +  H1 * (Xref1 - X1)
        +  H2 * (Xref2 - X2)
        +  H3 * (Xref3 - X3) 
        +  H4 * (Xref4 - X4);
  }

  T C0 = make_vector<P>(filterobjects.C[0]...);
  T C1 = make_vector<P>(filterobjects.C[1]...);
  T C2 = make_vector<P>(filterobjects.C[2]...);
  T C3 = make_vector<P>(filterobjects.C[3]...);
  T C4 = make_vector<P>(filterobjects.C[4]...);
  T C5 = make_vector<P>(filterobjects.C[5]...);
  T C6 = make_vector<P>(filterobjects.C[6]...);
  T C7 = make_vector<P>(filterobjects.C[7]...);
  T C8 = make_vector<P>(filterobjects.C[8]...);
  T C9 = make_vector<P>(filterobjects.C[9]...);
  T C10 = make_vector<P>(filterobjects.C[10]...);
  T C11 = make_vector<P>(filterobjects.C[11]...);
  T C12 = make_vector<P>(filterobjects.C[12]...);
  T C13 = make_vector<P>(filterobjects.C[13]...);
  T C14 = make_vector<P>(filterobjects.C[14]...);

  const T CHT0 = C0*H0 + C1*H1 + C3*H2 + C6*H3 + C10*H4;
  const T CHT1 = C1*H0 + C2*H1 + C4*H2 + C7*H3 + C11*H4;
  const T CHT2 = C3*H0 + C4*H1 + C5*H2 + C8*H3 + C12*H4;
  const T CHT3 = C6*H0 + C7*H1 + C8*H2 + C9*H3 + C13*H4;
  const T CHT4 = C10*H0 + C11*H1 + C12*H2 + C13*H3 + C14*H4;
  errorRes2 += H0*CHT0 + H1*CHT1 + H2*CHT2 + H3*CHT3 + H4*CHT4;

  const T w0 = res/errorRes2;
  X0 += CHT0 * w0;
  X1 += CHT1 * w0;
  X2 += CHT2 * w0;
  X3 += CHT3 * w0;
  X4 += CHT4 * w0;

  const T w1 = 1./errorRes2;
  C0 -= w1 * CHT0 * CHT0;
  C1 -= w1 * CHT1 * CHT0;
  C3 -= w1 * CHT2 * CHT0;
  C6 -= w1 * CHT3 * CHT0;
  C10 -= w1 * CHT4 * CHT0;
  C2 -= w1 * CHT1 * CHT1;
  C4 -= w1 * CHT2 * CHT1;
  C7 -= w1 * CHT3 * CHT1;
  C11 -= w1 * CHT4 * CHT1;
  C5 -= w1 * CHT2 * CHT2;
  C8 -= w1 * CHT3 * CHT2;
  C12 -= w1 * CHT4 * CHT2;
  C9 -= w1 * CHT3 * CHT3;
  C13 -= w1 * CHT4 * CHT3;
  C14 -= w1 * CHT4 * CHT4;

  const T chi2 = res*res / errorRes2;
}

// This version requires 32 registers
template<class ... FilterObject>
__attribute__ ((noinline)) void restrictedFilterDouble (FilterObject&... filterobjects) {

  Vec2d X0 (filterobjects.X[0]...);
  Vec2d X1 (filterobjects.X[1]...);
  Vec2d X2 (filterobjects.X[2]...);
  Vec2d X3 (filterobjects.X[3]...);
  Vec2d X4 (filterobjects.X[4]...);

  Vec2d res (filterobjects.refResidual...);
  Vec2d errorRes2 (filterobjects.errorMeas2...);

  const Vec2d H0 (filterobjects.H[0]...);
  const Vec2d H1 (filterobjects.H[1]...);
  const Vec2d H2 (filterobjects.H[2]...);
  const Vec2d H3 (filterobjects.H[3]...);
  const Vec2d H4 (filterobjects.H[4]...);

  {
    const Vec2d Xref0 (filterobjects.Xprev[0]...);
    const Vec2d Xref1 (filterobjects.Xprev[1]...);
    const Vec2d Xref2 (filterobjects.Xprev[2]...);
    const Vec2d Xref3 (filterobjects.Xprev[3]...);
    const Vec2d Xref4 (filterobjects.Xprev[4]...);

    res += H0 * (Xref0 - X0) 
        +  H1 * (Xref1 - X1)
        +  H2 * (Xref2 - X2)
        +  H3 * (Xref3 - X3) 
        +  H4 * (Xref4 - X4);
  }

  Vec2d C0 (filterobjects.C[0]...);
  Vec2d C1 (filterobjects.C[1]...);
  Vec2d C2 (filterobjects.C[2]...);
  Vec2d C3 (filterobjects.C[3]...);
  Vec2d C4 (filterobjects.C[4]...);
  Vec2d C5 (filterobjects.C[5]...);
  Vec2d C6 (filterobjects.C[6]...);
  Vec2d C7 (filterobjects.C[7]...);
  Vec2d C8 (filterobjects.C[8]...);
  Vec2d C9 (filterobjects.C[9]...);
  Vec2d C10 (filterobjects.C[10]...);
  Vec2d C11 (filterobjects.C[11]...);
  Vec2d C12 (filterobjects.C[12]...);
  Vec2d C13 (filterobjects.C[13]...);
  Vec2d C14 (filterobjects.C[14]...);

  const Vec2d CHT0 = C0*H0 + C1*H1 + C3*H2 + C6*H3 + C10*H4;
  const Vec2d CHT1 = C1*H0 + C2*H1 + C4*H2 + C7*H3 + C11*H4;
  const Vec2d CHT2 = C3*H0 + C4*H1 + C5*H2 + C8*H3 + C12*H4;
  const Vec2d CHT3 = C6*H0 + C7*H1 + C8*H2 + C9*H3 + C13*H4;
  const Vec2d CHT4 = C10*H0 + C11*H1 + C12*H2 + C13*H3 + C14*H4;
  errorRes2 += H0*CHT0 + H1*CHT1 + H2*CHT2 + H3*CHT3 + H4*CHT4;

  const Vec2d w0 = res/errorRes2;
  X0 += CHT0 * w0;
  X1 += CHT1 * w0;
  X2 += CHT2 * w0;
  X3 += CHT3 * w0;
  X4 += CHT4 * w0;

  const Vec2d w1 = 1./errorRes2;
  C0 -= w1 * CHT0 * CHT0;
  C1 -= w1 * CHT1 * CHT0;
  C3 -= w1 * CHT2 * CHT0;
  C6 -= w1 * CHT3 * CHT0;
  C10 -= w1 * CHT4 * CHT0;
  C2 -= w1 * CHT1 * CHT1;
  C4 -= w1 * CHT2 * CHT1;
  C7 -= w1 * CHT3 * CHT1;
  C11 -= w1 * CHT4 * CHT1;
  C5 -= w1 * CHT2 * CHT2;
  C8 -= w1 * CHT3 * CHT2;
  C12 -= w1 * CHT4 * CHT2;
  C9 -= w1 * CHT3 * CHT3;
  C13 -= w1 * CHT4 * CHT3;
  C14 -= w1 * CHT4 * CHT4;

  const Vec2d chi2 = res*res / errorRes2;
}
