
#include "vectorclass/vectorclass.h"

inline double dot5_avx(Vec4d f0, double f1, Vec4d  r0,  double r1) {
  return horizontal_add(r0*f0)+r1*f1;
}

inline double dot5_avx(const double* f, Vec4d  r0,  double r2) {
  return dot5_avx( Vec4d{}.load(f), f[4], r0, r2 );
}

struct alignas(32) avx_5_t {
  Vec4d c0,c1,c2,c3,c4; // r4 == c4...
  double c44 ;
  avx_5_t(const double* d)  :
    c0 { gather4d<0,1,3,6>(d) },
    c1 { gather4d<1,2,4,7>(d) },
    c2 { gather4d<3,4,5,8>(d) },
    c3 { gather4d<6,7,8,9>(d) },
    c4 { gather4d<10,11,12,13>(d) }, c44{ d[14] }
    { };
    // return a column of a rhs product with column-major f (aka. tranpose of row major f)
    template<int i=0, int j=i+1, int k=j+1, int l=k+1, int m=l+1>
    inline Vec4d c0i(const double* f ) const  {
      return c0*f[i]+c1*f[j]+c2*f[k]+c3*f[l]+c4*f[m];
    }
    inline double c4i(const double *f) const  {
      return dot5_avx( f, c4, c44 );
    }
};

__attribute__ ((noinline)) double filterVerticallyVectorised (
  double* X,
  double* C,
  const double* Xref,
  const double* H,
  double refResidual,
  double errorMeas2
  ) {
  // see Agner Fog's optimization guide, 12.1 about mixing AVX and non-AVX code,
  // (http://www.agner.org/optimize/optimizing_cpp.pdf)
  // and preserving the YMM register state.
  // Invoking __mm256_zeroupper seems to reduce the overhead when switching.
  // _mm256_zeroupper();

  const avx_5_t c(C);
  const auto cht0 = c.c0i(H);
  const auto cht4 = c.c4i(H);
  //TODO: combine
  const auto  res        = refResidual + dot5_avx(H,Vec4d{}.load(Xref)-Vec4d{}.load(X),Xref[4] - X[4]);
  const auto  errorRes2  = errorMeas2  + dot5_avx(H,cht0,cht4);

  const auto w = 1./errorRes2;
  const auto chtw0 = cht0*w;
  const auto chtw4 = cht4*w;
  // update the state vector and cov matrix
#if 1
  // keep this version for now, as it doesn't change the results...
  const auto wres = res/errorRes2;
  // const auto wres = w*res; // even this already changes the results...
  const auto x0 = Vec4d{}.load(X) + cht0*wres;
  const auto x4 = X[4]            + cht4*wres;
#else
  // this version has one division less, but produces slightly different answers... 
  // just by changing the equivalent of a*(b/c) into (a*b)/c...
  const auto x0 = Vec4d{}.load(X) + chtw0*res;
  const auto x4 = X[4]            + chtw4*res;
#endif

  const auto _0 = c.c0-cht0*chtw0[0];
  const auto _1 = blend4d<4,1,2,3>(c.c1, permute4d<1,0,2,2>(c.c4)) - Vec4d{cht4, cht0[1],cht0[2],cht0[3]} *chtw0[1];
  const auto _2 = blend4d<2,3,6,4>(c.c2,c.c4) - Vec4d{cht0[2],cht0[3],cht4,cht4}*permute4d<2,2,2,0>(chtw0);
  const auto _3 = Vec4d{c.c3[3],c.c4[3],c.c44,0.} - Vec4d{cht0[3],cht4,cht4,0.}*Vec4d{chtw0[3],chtw0[3],chtw4,0.};

  x0.store(X);
  X[4] = x4;

  blend4d<0,1,5,2>(_0,_1).store(C+0);
  blend4d<2,5,6,3>(_1,blend4d<-256,0,7,-256>(_2,_0)).store(C+4);
  blend4d<1,5,3,7>(_2,blend4d<-256,0,-256,4>(_3,_1 )).store(C+8);
  blend4d<6,1,2,-256>(_3,_2).store_partial(3,C+12);

  return w*res*res;
}
