#!/bin/bash

# Executable should be passed by arguments
CALL=$1

# Get core count for one single CPU
CPU_NAME=`cat /proc/cpuinfo | grep "model name" | tail -1 | sed -e 's/model name\t: //'`
PACKAGES=$((`cat /proc/cpuinfo | awk '/^physical id/{print $4}' | tail -1` + 1))
CORE_COUNT=$((`numactl --show | grep physcpubind | awk '{print $NF}'` + 1))
NUMA_NODES=$((`numactl --show | grep ^cpubind | awk '{print $NF}'` + 1))

mkdir -p output
cd output
echo '' > average_times.txt

printf "Machine description: \n"\
" $CPU_NAME \n"\
" $PACKAGES packages, $NUMA_NODES NUMA nodes, $CORE_COUNT cores \n" | tee -a average_times.txt

# Run as many instances as needed to occupy all cores
printf "\nStarting $CORE_COUNT processes\n"\
" $CALL\n " | tee -a average_times.txt

for i in `seq 1 $CORE_COUNT`
do
  printf "." | tee -a average_times.txt
  mkdir -p $i
  cd $i
  $CALL > output.txt 2>&1 &
  cd ..
done
printf "\n" | tee -a average_times.txt

cd ..
wait

# Parse results when complete
cd output
# Calculate average times
for i in `seq 1 $CORE_COUNT`
do
  cat $i/output.txt | grep "Sequential d" | awk '{print $7}' >> 1d_times.txt
  cat $i/output.txt | grep "Sequential f" | awk '{print $7}' >> 1f_times.txt
  cat $i/output.txt | grep "Vectorfilter_v1 2d" | awk '{print $6}' >> 2d_times.txt
  cat $i/output.txt | grep "Vectorfilter_v1 4f" | awk '{print $6}' >> 4f_times.txt
done
TIME_AVG_1D=`cat 1d_times.txt | awk '{sum += $1} END {print sum / NR}'`
TIME_AVG_1F=`cat 1f_times.txt | awk '{sum += $1} END {print sum / NR}'`
TIME_AVG_2D=`cat 2d_times.txt | awk '{sum += $1} END {print sum / NR}'`
TIME_AVG_4F=`cat 4f_times.txt | awk '{sum += $1} END {print sum / NR}'`

printf "\nTime averages:\n"\
"1d: $TIME_AVG_1D\n"\
"1f: $TIME_AVG_1F\n"\
"2d: $TIME_AVG_2D\n"\
"4f: $TIME_AVG_4F\n" | tee -a average_times.txt

# Attempt to get also throughput
# EVENTS_EXECUTED=`cat 1/output.txt | egrep "BrunelEventCount.*events processed" | awk '{print $3}'`
# echo $EVENTS_EXECUTED $SINGLENODE_CORE_COUNT $RECO_TIME_AVG $NUMA_NODES | awk '{print "Machine reconstruction throughput:", $4, "x", ($1*$2)/$3, "events/s"}' | tee -a average_times.txt

cd ..
