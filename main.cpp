
#include <iostream>
#include <fstream>
#include <vector>
#include <array>
#include <string>
#include <cmath>
#include <unistd.h> // getopt
#include "vectorclass/vectorclass.h"
#include "VectorRestrictedRegFilter.h"
#include "VectorGenericFilter.h"
#include "IntrinsicsFilter.h"
#include "Timer.h"
#include "Tools.h"
#include "FilterAlgorithm.h"

int main(int argc, char **argv) {
  std::vector<Timer> t (10);
  std::string filename = "filter.dat";
  std::vector<Filterd> input;
  long long int size;
  std::vector<double> maxEpsilons = {0.0, 0.0, 0.0};
  bool allcorrect = true;
  int numExperiments = 100;
  double acceptable_epsilon = 1e-5;
  TimePrinter p;
  char c;

  while ((c = getopt (argc, argv, "e:f:n:h?")) != -1) {
    switch (c) {
      case 'n':
        numExperiments = atoi(optarg);
        break;
      case 'f':
        filename = optarg;
        break;
      case 'e':
        acceptable_epsilon = atof(optarg);
        break;
      case 'h':
      case '?':
        std::cout << "vectorised_filter_test_suite [-f filename=filter.dat] [-n numExperiments=100] [-e acceptableEpsilonChi2=1e-5]" << std::endl;
        return 0;
      default:
        abort();
    }
  }

  std::cout << "Invoked with options: -n " << numExperiments << " -f " << filename << " -e " << acceptable_epsilon << std::endl;

  // Little matters - resize to modulo 16
  readBinary (filename, size, input);
  input.resize(input.size() - input.size() % 16);
  std::vector<Filterf> finput;
  for (auto& i : input) finput.push_back(Filterf(i));
  std::cout << "There are " << input.size() << " instances (mod 16) in this file." << std::endl << std::endl;

  // Setup our tests
  FilterSequential<double> seqFilter (input);
  FilterSequential<float> floatSeqFilter (finput);
  FilterOriginalIntrinsics originalIntrinsics (input);

  // Assume SSE is supported
  FilterVectorised<double, Filterd, Filterd> originalvec2d (input, invokeFilter);
  FilterVectorised<double, Filterd, Filterd> optFilter2d (input, optimisedFilter);
  FilterVectorised<double, Filterd, Filterd> moroptFilter2d (input, moreOptimisedFilter);
  FilterVectorised<float, Filterf, Filterf, Filterf, Filterf> originalvec4f (finput, invokeFilter);
  FilterVectorised<float, Filterf, Filterf, Filterf, Filterf> optFilter4f (finput, optimisedFilter);
  FilterVectorised<float, Filterf, Filterf, Filterf, Filterf> moroptFilter4f (finput, moreOptimisedFilter);

  double chi2d = FILTER(input[15274]);
  float  chi2f = FILTER(finput[15274]);

  // Warm up CPU
  for (int i=0; i<numExperiments; ++i) {
    seqFilter();
  }
  seqFilter.resetTimers();

  // Repeat experiment i times
  for (int i=0; i<numExperiments; ++i) {
    seqFilter();
    floatSeqFilter();
    originalvec2d();
    originalIntrinsics();
    originalvec4f();
    optFilter2d();
    optFilter4f();
    moroptFilter2d();
    moroptFilter4f();
  }

  // Check results of all double implementations
  int bad_results = 0;
  for (int i=0; i<input.size(); ++i) {
    if (!checkResult(input[0], input[0], seqFilter.chi2[i], originalIntrinsics.chi2[i], maxEpsilons, true, acceptable_epsilon)) bad_results++;
    if (!checkResult(input[0], input[0], seqFilter.chi2[i], originalvec2d.chi2[i], maxEpsilons, true, acceptable_epsilon)) bad_results++;
    if (!checkResult(input[0], input[0], seqFilter.chi2[i], optFilter2d.chi2[i], maxEpsilons, true, acceptable_epsilon)) bad_results++;
    if (!checkResult(input[0], input[0], seqFilter.chi2[i], moroptFilter2d.chi2[i], maxEpsilons, true, acceptable_epsilon)) bad_results++;
  }
  std::cout << "Double floating point precision checks:" << std::endl;
  std::cout << " Bad result counter (chi2 epsilon over " << acceptable_epsilon << "): " << bad_results << std::endl;
  std::cout << " Max epsilon with double precision (chi2): " << maxEpsilons[2] << std::endl << std::endl;
  
  maxEpsilons[2] = 0.0;
  bad_results = 0;
  bool found = 0;
  for (int i=0; i<input.size(); ++i) {
    if (!checkResult(input[0], finput[0], seqFilter.chi2[i], floatSeqFilter.chi2[i], maxEpsilons, true, acceptable_epsilon)) bad_results++;
  }
  std::cout << "Single floating point precision checks:" << std::endl;
  std::cout << " Bad result counter (chi2 epsilon over " << acceptable_epsilon << "): " << bad_results << std::endl;
  std::cout << " Max epsilon with single precision (chi2): " << maxEpsilons[2] << std::endl << std::endl;
  std::cout << "Timers:" << std::endl;

  // Check timers
  p.printTimer(seqFilter.timers, "Sequential d (auto-vectorised)", true);
  p.printTimer(floatSeqFilter.timers, "Sequential f (auto-vectorised)");
  p.printTimer(originalIntrinsics.timers, "__m128 2d intrinsics");
  p.printTimer(originalvec2d.timers, "Vectorfilter_v1 2d");
  p.printTimer(originalvec4f.timers, "Vectorfilter_v1 4f");
  p.printTimer(optFilter2d.timers, "Vectorfilter_v2 2d");
  p.printTimer(optFilter4f.timers, "Vectorfilter_v2 4f");
  p.printTimer(moroptFilter2d.timers, "Vectorfilter_v3 2d");
  p.printTimer(moroptFilter4f.timers, "Vectorfilter_v3 4f");

// AVX (2)
#if INSTRSET >= 7
  FilterVerticallyVectorised verticalFilter (input);
  FilterIntrinsics intrinsicsFilter (input);
  FilterVectorised<double, Filterd, Filterd, Filterd, Filterd> originalvec4d (input, invokeFilter);
  FilterVectorised<double, Filterd, Filterd, Filterd, Filterd> optFilter4d (input, optimisedFilter);
  FilterVectorised<double, Filterd, Filterd, Filterd, Filterd> moroptFilter4d (input, moreOptimisedFilter);
  FilterVectorised<float, Filterf, Filterf, Filterf, Filterf, Filterf, Filterf, Filterf, Filterf> originalvec8f (finput, invokeFilter);
  FilterVectorised<float, Filterf, Filterf, Filterf, Filterf, Filterf, Filterf, Filterf, Filterf> optFilter8f (finput, optimisedFilter);
  FilterVectorised<float, Filterf, Filterf, Filterf, Filterf, Filterf, Filterf, Filterf, Filterf> moroptFilter8f (finput, moreOptimisedFilter);

  for (int i=0; i<numExperiments; ++i) {
    intrinsicsFilter();
    verticalFilter();
    originalvec4d();
    originalvec8f();
    optFilter4d();
    optFilter8f();
    moroptFilter4d();
    moroptFilter8f();
  }
  
  p.printTimer(verticalFilter.timers, "Vertically vectorised d (Gerhard)");
  p.printTimer(intrinsicsFilter.timers, "__m256 2d intrinsics");
  p.printTimer(originalvec4d.timers, "Vectorfilter_v1 4d");
  p.printTimer(originalvec8f.timers, "Vectorfilter_v1 8f");
  p.printTimer(optFilter4d.timers, "Vectorfilter_v2 4d");
  p.printTimer(optFilter8f.timers, "Vectorfilter_v2 8f");
  p.printTimer(moroptFilter4d.timers, "Vectorfilter_v3 4d");
  p.printTimer(moroptFilter8f.timers, "Vectorfilter_v3 8f");
#endif

// AVX 512
#if INSTRSET >= 9
  FilterVectorised<double, Filterd, Filterd, Filterd, Filterd, Filterd, Filterd, Filterd, Filterd> originalvec8d (input, invokeFilter);
  FilterVectorised<float, Filterf, Filterf, Filterf, Filterf, Filterf, Filterf, Filterf, Filterf,
                          Filterf, Filterf, Filterf, Filterf, Filterf, Filterf, Filterf, Filterf> originalvec16f (finput, invokeFilter);
  FilterVectorised<double, Filterd, Filterd, Filterd, Filterd, Filterd, Filterd, Filterd, Filterd> optFilter8d (input, optimisedFilter);
  FilterVectorised<float, Filterf, Filterf, Filterf, Filterf, Filterf, Filterf, Filterf, Filterf,
                          Filterf, Filterf, Filterf, Filterf, Filterf, Filterf, Filterf, Filterf> optFilter16f (finput, optimisedFilter);
  FilterVectorised<double, Filterd, Filterd, Filterd, Filterd, Filterd, Filterd, Filterd, Filterd> moroptFilter8d (input, moreOptimisedFilter);
  FilterVectorised<float, Filterf, Filterf, Filterf, Filterf, Filterf, Filterf, Filterf, Filterf,
                          Filterf, Filterf, Filterf, Filterf, Filterf, Filterf, Filterf, Filterf> moroptFilter16f (finput, moreOptimisedFilter);

  for (int i=0; i<numExperiments; ++i) {
    originalvec8d();
    originalvec16f();
    optFilter8d();
    optFilter16f();
    moroptFilter8d();
    moroptFilter16f();
  }

  p.printTimer(originalvec8d.timers, "Vectorfilter_v1 8d");
  p.printTimer(originalvec16f.timers, "Vectorfilter_v1 16f");
  p.printTimer(optFilter8d.timers, "Vectorfilter_v2 8d");
  p.printTimer(optFilter16f.timers, "Vectorfilter_v2 16f");
  p.printTimer(moroptFilter8d.timers, "Vectorfilter_v3 8d");
  p.printTimer(moroptFilter16f.timers, "Vectorfilter_v3 16f");
#endif

  return 0;
}
