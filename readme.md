# Vectorisation study for the Kalman Filter LHCb::Math::filter function


Things should work right away. You'll need vectorclass by Agner Fog, to be placed
in a "vectorclass" folder (http://www.agner.org/optimize/#vectorclass).

In SLC6 nodes, it can be compiled with:

    g++ *cpp -std=c++11 -mavx -mfma -O2 -fabi-version=6 -lrt

From CentOS 7 on, -mavx2 or -mavx512 generated instructions are supported by the assembler (/usr/bin/as).

With a newer g++, the following works:

    g++ *cpp -std=c++11 -mavx2 -mfma -O2

